
// Major scale functionality

// target  all the major buttons
const majButtons = document.querySelectorAll('.majButton'); // comment later
// loop through the list
for (const majButton of majButtons) {
    // attach the onClick event
    majButton.addEventListener('click',displayScaleMaj);
}



// handle the onclick event
function displayScaleMaj() {

    //data to be displayed when button in major key is clicked
    const scalesMaj = {
        majKey1: 'C D E F G A B',
        majKey2: 'G A B C D E F#',
        majKey3: 'D E F# G A B C#',
        majKey4: 'A B C# D E F# G#',
        majKey5: 'E F# G# A B C# D#',
        majKey6: 'B C# D# E F# G# A#',
        majKey7: 'F# G# A# B C# D# E#',
        majKey8: 'C# D# E# F# G# A# B#',
        majKey9: 'F G A Bb C D E ',
        majKey10:' Bb C D Eb F G A ',
        majKey11:' Eb F G Ab Bb C D ',
        majKey12:' Ab Bb C Db Eb F G ',
        majKey13:' Db Eb F Gb Ab Bb C ',
        majKey14:' Gb Ab Bb Cb Db Eb F ',
        majKey15:'Cb Db Eb Fb Gb Ab Bb ',


    }

    // get scaleKeyMaj value from button that was clicked,
    const scaleKeyMaj = this.value;
    // get the scale that is associated with the specific value
    const scaleMaj = scalesMaj[scaleKeyMaj];
    // put the scale in the dome
    const userScaleMaj = document.getElementById('scale-area-maj');
    userScaleMaj.innerHTML = scaleMaj;
    console.log(scaleMaj);
};



// Minor Scale functionality

// target all the minor key buttons
    const minButtons =  document.querySelectorAll('.minButton');
// loop through the list
    for(const minButton of minButtons) {
        // attach the onClick event
        minButton.addEventListener('click',displayScaleMin);
    }

 //Handle the onClick event with minor keys
    function displayScaleMin() {
        // data to be displayed when button in minor key is clicked
        const scalesMin = {
            minKey1: 'C D Eb F G Ab Bb ',
            minKey2: 'G A Bb C D Eb ',
            minKey3: 'D E F G A Bb ',
            minKey4: 'A B C D E F G ',
            minKey5: 'E F# G A B C D ',
            minKey6: 'B C# D E F# G A ',
            minKey7: 'F# G# A B C# D E F',
            minKey8: 'C# D# E F# G# A B C',
            minKey9: 'F G Ab Bb C Db Eb ',
            minKey10:'Bb C Db Eb F Gb Ab,',
            minKey11:'Eb F Gb Ab Bb Cb Db E ',
            minKey12:'Ab Bb Cb Db Eb Fb Gb A ',
            minKey13:'Db Eb Fb Gb Ab A B  ',
            minKey14:'Gb Ab Bbb Cb Db Ebb F ',
            minKey15:'Cb Db D E F# G A  ',
        }
        // get scaleKeyMin value from the button that was clicked
            const scaleKeyMin = this.value;
        // set const scalesMin value to the selected min button,
            const scaleMin = scalesMin[scaleKeyMin];
        // put the scale in the dome
            const userScaleMin = document.getElementById('scale-area-min');
            userScaleMin.innerHTML = scaleMin;
            console.log(scaleMin);

    };


    // Fret Board Functionality

        /* Tonic and Root Functionality */
            // need to create a function of a button
            const keySelected = document.querySelectorAll('.btn'); // ask why I was getting undefined when missing the .btn deceleration?
            // Determine which button was selected
            for(const keySelect of keySelected){
                keySelect.addEventListener('click',displayRootFunction);
            }
            // change the display of the content inside the
            function displayRootFunction() {
                const rootVale = {
                    key1:'C D E F G A B',
                    key2:'C# D# E# F# G# A# B#',
                    key3: 'D E F# G A B C#',
                    key4: 'Eb F G Ab Bb C D',
                    key5: 'E F# G# A B C# D#',
                    key6: 'F G A Bb C D E',
                    key7: 'F# G# A# B C# D# E#',
                    key8: 'G A B C D E F#',
                    key9: 'Ab Bb C Db Eb F G',
                    key10:'A B C# D E F# G#',
                    key11: 'Bb C D Eb F G A',
                    key12: 'B C# D# E F# G# A#',

                }
                // get the key values for the root value selected by the user
                const rootSelect = this.value;
                // gather the info from the root value that was selected
                const rootVal = rootVale[rootSelect];

                // print the value of the key selected into the dom in the td id=rootTonic
                const userRootSelect = document.getElementById('rootTonic');
                userRootSelect.innerHTML = rootVal;
                console.log(rootVal);
            };


